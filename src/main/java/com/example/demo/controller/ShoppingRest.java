package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Item;

@RestController
public class ShoppingRest {

    @Autowired
    private CartService cartService;

    @PostMapping(path = "/add/{name}/{price}")
    public void addNewItem(@PathVariable String name, @PathVariable int price) {
        Item item = new Item();
        item.setName(name);
        item.setPrice(price);

        cartService.addItem(item);
    }
}
