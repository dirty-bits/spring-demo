package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.model.Cart;
import com.example.demo.model.Item;
import com.example.demo.model.dao.ItemRepository;

@Service
public class CartService {
    
    @Autowired
    private ItemRepository itemRepository;
    
    private Cart cart;
    
    public CartService() {
        cart = new Cart();
    }
    
    public void addItems(List<Item> items) {
        items.forEach(item -> itemRepository.save(item));
    }

    public void addItem(Item item) {
        itemRepository.save(item);
    }
}
