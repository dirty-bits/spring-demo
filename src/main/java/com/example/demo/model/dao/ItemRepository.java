package com.example.demo.model.dao;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.model.Item;

@Repository
public interface ItemRepository extends MongoRepository<Item, String> {

    List<Item> findByNameEquals(@Param("name") String name);
}
